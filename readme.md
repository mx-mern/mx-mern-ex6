# Mindx MERN Course
## Day 6
1. Tạo 1 trang xem danh sách các user
2. Tạo 1 form tạo / sửa user
3. Làm chức năng xoá user trong danh sách user

## Day 5
### API: Dựa theo data mẫu trong file đính kèm
  - Get list user
  `http://localhost:3000/user/list`
  - Tìm kiếm user theo tên (chấp nhận kết quả gần đúng)
  `http://localhost:3000/user/find?q=Toàn`
  - Tạo thêm user (Dữ liệu yêu cầu username, password)
  ```js
  http://localhost:3000/user/create
  {"username": "Thinh Nguyen", "password": "123456"}
  ```
  - Cập nhật user theo id
  ```js
  localhost:3000/user/update/0
  {"username": "Thinh Nguyen"}
  ```
  - Xoá user
  `localhost:3000/user/delete/1`