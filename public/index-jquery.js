$(document).ready(function() {
    $.get("/user/list", function(response, status){
        console.log("response.data: " + JSON.stringify(response.data)+ "\nStatus: " + status);
        for (item of response.data) {
            var delButton= $('<input type="button" class="delete-button" value="Delete"/>');
            var editButton= $('<input type="button" class="edit-button" value="Edit"/>');
            $('#user-table').find('tbody').append(
                $('<tr>')
                    .append($('<th scope="row" class="userid">').append(item.id))
                    .append($('<td class="username">').append(item.username))
                    .append($('<td>').append(delButton))
                    .append($('<td>').append(editButton))
            );
        }
    });

    $(document).on("click", ".delete-button" , function() {
        console.log('Delete Button clicked');
        var username = $( this ).parent().siblings("td.username").text();
        var userid = $( this ).parent().siblings("th.userid").text();
        let tr = $( this ).parent().parent();
        console.log("username: ", username, "user id: ", userid);
        var ret = confirm("Are you sure to delete user: " + username);
        if (ret == true) {
            console.log("You pressed OK!");
            if (userid && username) {
                // TODO: delete the 
                var deleteAPI = `/user/delete/${userid}`;
                console.log(`Prepare to post: ${deleteAPI}`);
                $.post(`/user/delete/${userid}`, function(data) {
                    console.log("Deleted response: " + JSON.stringify(data));
                    if (data.success == true) {
                        console.log("Update the table (remove row)");
                        tr.remove();
                    }
                });
            }
        } else {
            console.log("You pressed Cancel!");
        }
    });

    $(document).on("click", ".edit-button" , function() {
        console.log('Edit Button clicked');
        var username = $( this ).parent().siblings("td.username").text();
        var userid = $( this ).parent().siblings("th.userid").text();
        let tr = $( this ).parent().parent();
        console.log("username: ", username, "user id: ", userid);
        $('#old_username').val(username);
        $('.edit-group').css('display', 'block');
        $(document).on("click", "#update-username" , function() {
            console.log('Update username clicked');
            if ($('#new_username').val()) 
            {
                console.log("New username" + $('#new_username').val());
                $.ajax({
                    url: `/user/update/${userid}`,
                    type: 'PUT',
                    data: { username: $('#new_username').val() },
                    success: function (rsp) {
                        console.log("Update user response: " + JSON.stringify(rsp));
                        if (rsp.success == true) {
                            console.log("Add new user response OK");
                            $('.edit-group').css('display', 'none');
                            location.reload(true);
                        }
                        else {
                            alert("Update failed");
                        }
                    }
                });

                // $.put(`/user/update/${userid}`, { username: $('#new_username').val()})
                // .done(function (data) {
                //     console.log("Update user response: " + JSON.stringify(data));
                //     if (data.success == true) {
                //         console.log("Add new user response OK");
                //         $('.edit-group').css('display', 'none');
                //         location.reload(true);
                //     }
                //     else {
                //         alert("Update failed");
                //     }
                // });
            } else {
                // $('.edit-group').css('display', 'none');
                alert("New Username is empty");
            }
            
        });

        $(document).on("click", "#update-username-cancel" , function() {
            console.log('Update username cancel clicked');
        $('.edit-group').css('display', 'none');
        });
    });

    $(document).on("click", "#new-user-btn" , function() {
        console.log('New User Button clicked');
        let reg_username = $('#reg_username').val();
        let reg_password = $('#reg_password').val();
        let reg_password_confirm = $('#reg_password_confirm').val();
        console.log('reg_username: ' + reg_username);
        console.log('reg_password: ' + reg_password);
        console.log('reg_password_confirm: ' + reg_password_confirm);
        if (reg_username) {
            if (reg_password)
            {
                if (reg_password == reg_password_confirm)
                {
                    alert("New user added");
                    $.post("/user/create",{username: reg_username, password: reg_password})
                        .done(function(data) {
                        console.log("Add new user response: " + JSON.stringify(data));
                        if (data.success == true) {
                            console.log("Add new user response OK");
                            location.reload(true);
                        }
                    });
                } else {
                    alert("Password not match");
                }   
            } else {
                alert("Password is empty");
            }

        } else {
            alert("Username is empty");
        }
    });
})