const express = require('express');
const app = express();
const port = 3000;
const bodyparser = require('body-parser');
// var formidable = require('formidable');

const router = require('./user')

app.use(bodyparser.urlencoded());

app.use('/public',express.static('public'));

app.use('/user', router);

app.listen(port, () => console.log(`Mx-Mern-Ex6 app listening on port ${port}!`))